let our_Input = document.getElementById('emailInput')

our_Input.onfocus = function () {
    'use strict';
    if (this.placeholder === 'support@freshmeal.com'){
        this.placeholder = '';
    }
};
our_Input.onblur = function () {
    'use strict';
    if (this.placeholder === ''){
        this.placeholder = 'support@freshmeal.com';
    }
};

//jQuery Toggle function

/*$(document).ready(function () {
    $('.nav-trigger').click(function () {
        $('.site-content-wrap').toggleClass('scaled');

    })

});*/

// JavaScript toggle function
const siteContentWrap = document.querySelector('.site-content-wrap');
document.querySelector('.nav-trigger').onclick = function () {
    siteContentWrap.classList.toggle('scaled');

}

